#pragma once 
#include <Windows.h>

void LoadPlistFromFile(LPCTSTR lpFileName, TCHAR szRet[MAX_PATH]);
BOOL GetCarrierFromICCID(LPCTSTR lpICCID, TCHAR szRet[MAX_PATH]);