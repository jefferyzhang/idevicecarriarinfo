this data ICCID to carrier from ftp server
iPhone 3GS ICCID

iPhone 4 Sprint
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>CarrierBundleName</key>
	<string>310SPR</string>
	<key>CarrierId</key>
	<string>310SPR</string>
	<key>LASDNextUpdate</key>
	<date>2015-04-27T21:24:28Z</date>
	<key>NextUpdate</key>
	<date>2015-04-24T17:11:32Z</date>
	<key>WaitingForActivationFlag</key>
	<true/>
</dict>
</plist>

iPhone 4 Verizon
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>CarrierBundleName</key>
	<string>310VZW</string>
	<key>CarrierId</key>
	<string>310VZW</string>
	<key>LASDNextUpdate</key>
	<date>2015-04-29T01:22:15Z</date>
	<key>NextUpdate</key>
	<date>2015-04-23T17:17:57Z</date>
	<key>WaitingForActivationFlag</key>
	<true/>
</dict>
</plist>

iPhone 4S CDMA
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>CarrierBundleName</key>
	<string>Unknown.bundle</string>
	<key>CarrierEntitlementsControllerNextUpdate</key>
	<date>2015-04-15T01:14:07Z</date>
	<key>CarrierId</key>
	<string>310VZW</string>
	<key>LASDNextUpdate</key>
	<date>2015-04-26T17:12:37Z</date>
	<key>PhoneNumber</key>
	<string></string>
	<key>PhoneNumberChangeReport</key>
	<true/>
	<key>WaitingForActivationFlag</key>
	<true/>
	<key>Wallet</key>
	<dict>
		<key>RoamingHandler</key>
		<dict>
			<key>EffectiveId</key>
			<string>89011200000120618854</string>
		</dict>
		<key>SmsController</key>
		<dict>
			<key>SubscriberAccountId</key>
			<string>89011200000120618854</string>
		</dict>
		<key>VoiceMailController</key>
		<dict>
			<key>SubscriberAccountId</key>
			<string>89011200000120618854</string>
		</dict>
	</dict>
</dict>
</plist>

iPhone 4s Verizon
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>CarrierBundleName</key>
	<string>310VZW</string>
	<key>CarrierId</key>
	<string>310VZW</string>
	<key>WaitingForActivationFlag</key>
	<true/>
	<key>Wallet</key>
	<dict>
		<key>RoamingHandler</key>
		<dict>
			<key>EffectiveId</key>
			<string>8931440880698413888</string>
		</dict>
		<key>SmsController</key>
		<dict>
			<key>SubscriberAccountId</key>
			<string></string>
		</dict>
		<key>VoiceMailController</key>
		<dict>
			<key>SubscriberAccountId</key>
			<string></string>
		</dict>
	</dict>
</dict>
</plist>


