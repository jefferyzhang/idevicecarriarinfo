// iDeviceCarriarInfo.cpp : main project file.

#include "stdafx.h"
#include <Windows.h>

#include "cheadercommon.h"
#include "clicommon.h"
#include "Runexe.h"

using namespace System;
using namespace System::Threading;
using namespace System::Diagnostics;
using namespace System::IO;

static int _label = 0;



void logIt(String^ s)
{
	System::Diagnostics::Trace::WriteLine(String::Format("[Label_{0}]: {1}", _label, s));
}


String^ get_AppleInstallDir(String^ moduleName)
{
	String^ ret = String::Empty;
	try
	{
		String^ Keypath = "SOFTWARE\\Apple Inc.\\{0}";
		Microsoft::Win32::RegistryKey^ regKey = Microsoft::Win32::Registry::LocalMachine->OpenSubKey(String::Format(Keypath, moduleName));
		ret = (String^)regKey->GetValue("InstallDir");
		regKey->Close();
	}
	catch (...) {}
	return ret;
}

void downloadFileFromFtp(String^ remoteFilename, String^ localFilename)
{
	System::Threading::Mutex^ m = nullptr;
	bool needReleaseMutex = false;
	try
	{
		m = System::Threading::Mutex::OpenExisting(System::IO::Path::GetFileNameWithoutExtension(localFilename));
	}
	catch (Threading::WaitHandleCannotBeOpenedException^ e)
	{
		m = gcnew Threading::Mutex(false, System::IO::Path::GetFileNameWithoutExtension(localFilename));
	}
	catch (...)
	{
		m = nullptr;
	}
	try
	{
		if (m != nullptr)
			needReleaseMutex = m->WaitOne(10 * 1000);

		System::Boolean needDownload = false;
		if (!System::IO::File::Exists(localFilename))
			needDownload = true;
		else
		{
			System::IO::FileInfo^ info = gcnew System::IO::FileInfo(localFilename);
			System::TimeSpan^ ts = DateTime::Now - info->LastWriteTime;
			if (ts->TotalHours > 2)
			{
				needDownload = true;
			}
		}
		if (needDownload)
		{
			System::Net::WebClient^ client = gcnew Net::WebClient();
			client->Credentials = gcnew Net::NetworkCredential(L"fd_eng", L"FDeng");
			client->DownloadFile(remoteFilename, localFilename);
		}
	}
	catch (Exception^ e)
	{
	}
	finally
	{
		if (m != nullptr)
		{
			if (needReleaseMutex)
				m->ReleaseMutex();
			m->Close();
		}
	}
}

int main(array<System::String ^> ^args)
{
	String^ sAMDSDir = get_AppleInstallDir("Apple Mobile Device Support");
	Environment::SetEnvironmentVariable("Path", String::Join(";", gcnew array<String^> { Environment::GetEnvironmentVariable("Path"), get_AppleInstallDir("Apple Application Support"), sAMDSDir }));
	System::Configuration::Install::InstallContext^ _param = gcnew System::Configuration::Install::InstallContext(nullptr, args);

	String^ sUdid;
	if (_param->Parameters->ContainsKey("udid"))
	{
		sUdid = _param->Parameters["udid"];
	}
	if (_param->Parameters->ContainsKey("label"))
	{
		_label = Convert::ToInt32(_param->Parameters["label"]);
	}
	//sUdid = "8931c35c21b2c3212dc9e987698a9f2c66ca6813";

	downloadFileFromFtp(L"ftp://ftp2.futuredial.com/ModusLink/AppleConfig/CarrierBundle.ini",
		System::IO::Path::Combine(System::IO::Path::GetDirectoryName(System::Diagnostics::Process::GetCurrentProcess()->MainModule->FileName), L"CarrierBundle.ini"));

	if (_param->Parameters->ContainsKey("queryiccid"))
	{
		String^ iccid = _param->Parameters["queryiccid"];
		using namespace Runtime::InteropServices;
		const wchar_t* lpICCID =
			(const wchar_t*)(Marshal::StringToHGlobalUni(iccid)).ToPointer();
		TCHAR szRet[MAX_PATH] = { 0 };
		ZeroMemory(szRet, MAX_PATH);
		if (!GetCarrierFromICCID(lpICCID, szRet))
		{
			logIt("Can not file ICCID to Carrier.");
			return 1;
		}
		return 0;
	}

	if (String::IsNullOrEmpty(sUdid)) return 1;

	iDeviceCarriarInfo::Runexe^ aa = gcnew iDeviceCarriarInfo::Runexe();
	aa->DoTask(sUdid, sAMDSDir);
	
    return 0;
}
