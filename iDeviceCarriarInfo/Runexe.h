#pragma once

using namespace System;
using namespace System::Diagnostics;
using namespace System::Threading;
using namespace System::Text;

namespace iDeviceCarriarInfo {

	/// <summary>
	/// Summary for Runexe
	/// </summary>
	public ref class Runexe 
	{
	public:
		Runexe(void) 
		{
			_quit = false;
		}
		int startRunExe(String^ exepath, String^ sparam, int timeout);
		String^ getExeStdOut();

		int DoTask(String^ sUdid, String^ sAppleBackupPath);


	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Runexe()
		{
			
		}

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		static Boolean _quit;
		static AutoResetEvent^ runningEvent;
		static StringBuilder^  sbExeOut;
#pragma region  generated code
		static void DataReceived_Handler(Object^ sender, DataReceivedEventArgs^ e);
		static void iDeviceCarriarInfo::Runexe::Exited_startiDeviceUtilCore(Object^ sender, EventArgs^ e);
#pragma endregion
	};
}
