#include "stdafx.h"
#include <winerror.h>
#include "cheadercommon.h"
#include "Runexe.h"
#include "clicommon.h"
using namespace System::IO;

void iDeviceCarriarInfo::Runexe::Exited_startiDeviceUtilCore(Object^ sender, EventArgs^ e)
{
	logIt("Exited_startRunExe");
	_quit = true;
	runningEvent->Set();
}

void iDeviceCarriarInfo::Runexe::DataReceived_Handler(Object^ sender, DataReceivedEventArgs^ e)
{
	runningEvent->Set();
	if (!String::IsNullOrEmpty(e->Data))
	{
		String^ ss = e->Data;
		sbExeOut->AppendLine(ss);
		//logIt(ss);
	}
}

String^ iDeviceCarriarInfo::Runexe::getExeStdOut()
{
	return sbExeOut->ToString();
}

int iDeviceCarriarInfo::Runexe::startRunExe(String^ exepath, String^ sparam, int timeout)
{
	int ret = 0;
	_quit = false;
	logIt("startRunExe: ++ " + exepath);
	logIt("Run Start Time :" + DateTime::Now.ToString());
	runningEvent = gcnew AutoResetEvent(false);
	sbExeOut = gcnew StringBuilder();
	if (System::IO::File::Exists(exepath))
	{
		logIt(String::Format("launch exe with {0}", sparam));
		FileVersionInfo^ vinfo = FileVersionInfo::GetVersionInfo(exepath);
		logIt(String::Format("{0} version {1}", System::IO::Path::GetFileName(vinfo->FileName), vinfo->FileVersion));
		Process^ p = gcnew Process();
		p->StartInfo->FileName = exepath;
		p->StartInfo->Arguments = sparam;
		p->StartInfo->UseShellExecute = false;
		//p.StartInfo.RedirectStandardError = true;
		p->StartInfo->RedirectStandardOutput = true;
		p->Exited += gcnew EventHandler(Exited_startiDeviceUtilCore);
		////p.EnableRaisingEvents = true;

		p->OutputDataReceived += gcnew DataReceivedEventHandler(DataReceived_Handler);
		p->Start();
		p->BeginOutputReadLine();

		logIt(String::Format("{0}: pid={1} ", Path::GetFileName(vinfo->FileName), p->Id));
		while (!_quit && !p->HasExited)
		{
			if (!runningEvent->WaitOne(timeout))
			{
				try
				{
					if (!p->HasExited)
					{
						p->Kill();
					}
				}
				catch (...)
				{
				}
				_quit = true;
			}
		}
		if (!p->HasExited)
			p->WaitForExit();
		ret = p->ExitCode;
	}
	else
	{
		logIt("not Found exe file.");
		ret = 20011;
	}
	logIt("Run End Time :" + DateTime::Now.ToString());
	logIt(sbExeOut->ToString());
	logIt(String::Format("param: {0} return {1}", sparam, ret));
	return ret;
}

#define IDEVICEUTIL			L"iDeviceUtil.exe"
#define IDEVICEUTILCORE		L"iDeviceUtilCore.exe"

int iDeviceCarriarInfo::Runexe::DoTask(String^ sudid, String^ sAppleBackupPath)
{
	int ret = ERROR_SUCCESS;
	// backup
	//query
	//getcarrier bundler
	
	String^ sCurpath = System::AppDomain::CurrentDomain->BaseDirectory;
	String^ sCoreExe = Path::Combine(sCurpath, IDEVICEUTILCORE);
	String^ sUtilExe = Path::Combine(sCurpath, IDEVICEUTIL);

	TCHAR temppath[MAX_PATH];
	GetTempPath(MAX_PATH, (LPTSTR)&temppath);


	String ^ sTempPath = gcnew String(temppath);
	//String^ sTempPath = System::IO::Path::GetTempPath();
	if (sTempPath->LastIndexOf("\\"))
	{
		sTempPath = sTempPath->Substring(0, sTempPath->Length - 1);
	}

	String^ sBackupParam = String::Format("-u {0} --backup --local \"{1}\" --files \"/var/wireless/Library/Preferences/com.apple.commcenter.plist\"", sudid, sTempPath);
	String^ sBundleParam = String::Format("-infobundle -carrierlock -udid={0}", sudid);

	_quit = false;
	String^ sRet = String::Empty;

	startRunExe(sCoreExe, sBackupParam, 800 * 1000);
	String^ commcentrplist = Path::Combine(sTempPath, sudid, "Snapshot\\bfecaa9c467e3acb085a5b312bd27bdd5cd7579a");
	if (!File::Exists(commcentrplist))
	{
		try{
			Directory::Delete(Path::Combine(sTempPath, sudid), true);
		}
		catch (...)
		{
		}
		logIt("backup failed. can not find this plist, use AppleMobileBackup try");
		sBackupParam = String::Format(" --backup --device \"-1\" --target {0} -D WirelessDomain -q \"{1}\"", sudid, sTempPath);
		String^ sAppleMobileBackup = Path::Combine(sAppleBackupPath, "AppleMobileBackup.exe");
		startRunExe(sAppleMobileBackup, sBackupParam, 800 * 1000);
		commcentrplist = Path::Combine(sTempPath, sudid, "bfecaa9c467e3acb085a5b312bd27bdd5cd7579a");
	}
	if (File::Exists(commcentrplist))
	{
		using namespace Runtime::InteropServices;
		const wchar_t* ccmplist =
			(const wchar_t*)(Marshal::StringToHGlobalUni(commcentrplist)).ToPointer();
		TCHAR szRet[MAX_PATH] = { 0 };
		ZeroMemory(szRet, MAX_PATH);
		LoadPlistFromFile(ccmplist, szRet);
		if (wcslen(szRet) > 0)
		{
			sRet = gcnew String(szRet);
		}
		logIt(String::Format("get Carrier = {0}", sRet));
	}
	else
	{
		logIt("backup failed. ALL WAYS can not find this plist");
	}
	try{
		Directory::Delete(Path::Combine(sTempPath, sudid), true);
	}
	catch (...)
	{
	}

	_quit = false;
	startRunExe(sUtilExe, sBundleParam, 10 * 1000);
	logIt(sbExeOut->ToString());

	sRet += sbExeOut->ToString();



	System::Console::WriteLine(sRet);
	return ret;
}