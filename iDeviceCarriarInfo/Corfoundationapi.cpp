#include "stdafx.h"
#include "cheadercommon.h"
#include <atlstr.h>
#include <CoreFoundation/CFString.h>
#include <CoreFoundation/CFNumber.h>
#include <CoreFoundation/CFPropertyList.h>
#include <CoreFoundation/CFURLAccess.h>

LPTSTR ConvertData(CFStringRef val)
{
	CFIndex length = CFStringGetLength(val);
	CFIndex maxSize = CFStringGetMaximumSizeForEncoding(length,
		kCFStringEncodingUnicode);
	wchar_t *buffer = (wchar_t *)malloc(maxSize+sizeof(wchar_t));
	ZeroMemory(buffer, maxSize + sizeof(wchar_t));
	if (NULL != buffer)
	{
		if (CFStringGetCString(val, (char*)buffer, maxSize + sizeof(wchar_t), kCFStringEncodingUnicode))
		{
			
		}
		
	}
	return buffer;
}


BOOL GetCarrierFromICCID(LPCTSTR lpICCID, TCHAR szRet[MAX_PATH])
{
	TCHAR szIni[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, szIni, MAX_PATH);
	CString sIniPath = szIni;
	int i = sIniPath.ReverseFind(L'\\');
	szIni[i] = 0;
	BOOL bRet = FALSE;
	PathCombine(szIni, szIni, _T("CarrierBundle.ini"));
	for (int i = 5; i < 15; i++)
	{
		TCHAR szRetBuf[MAX_PATH] = { 0 };
		CString s = lpICCID;

		GetPrivateProfileString(_T("MobileDeviceCarriers"), s.Left(i).GetString(), _T(""), szRetBuf, MAX_PATH, szIni);
		if (_tcslen(szRetBuf) > 0)
		{

			TCHAR szKey[MAX_PATH] = { 0 };
			_stprintf_s(szKey, MAX_PATH, _T("com.apple.%s\0"), szRetBuf);
			GetPrivateProfileString(_T("carrierName"), szKey, _T("Unkown"), szRetBuf, MAX_PATH, szIni);
			wsprintf(szRet, L"LastCarrier=%s\n", szRetBuf);
			_tprintf(szRet);
			bRet = TRUE;
			break;
		}
		GetPrivateProfileString(_T("MobileDeviceCarriersByMccMnc"), s.Left(i).GetString(), _T(""), szRetBuf, MAX_PATH, szIni);
		if (_tcslen(szRetBuf) > 0)
		{
			TCHAR szKey[MAX_PATH] = { 0 };
			_stprintf_s(szKey, MAX_PATH, _T("com.apple.%s\0"), szRetBuf);
			GetPrivateProfileString(_T("carrierName"), szKey, _T("Unkown"), szRetBuf, MAX_PATH, szIni);
			wsprintf(szRet, L"LastCarrier=%s\n", szRetBuf);
			_tprintf(szRet);
			bRet = TRUE;
			break;
		}
	}
	return bRet;
}

void LoadPlistFromFile(LPCTSTR lpFileName, TCHAR szRet[MAX_PATH])
{
	CFPropertyListRef plistref;
	CFStringRef       errorString;
	CFDataRef         resourceData;
	Boolean           status;
	SInt32            errorCode;

	CFStringRef sFileName = CFStringCreateWithCharacters(NULL, (const UniChar*)lpFileName, wcslen(lpFileName));

	CFURLRef fileURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, sFileName, kCFURLWindowsPathStyle, false);
	// Read the XML file.
	status = CFURLCreateDataAndPropertiesFromResource(
		kCFAllocatorDefault,
		fileURL,
		&resourceData,            // place to put file data
		NULL,
		NULL,
		&errorCode);

	// Reconstitute the dictionary using the XML data.
	plistref = CFPropertyListCreateFromXMLData(kCFAllocatorDefault,
		resourceData,
		kCFPropertyListImmutable,
		&errorString);

	if (resourceData) {
		CFRelease(resourceData);
	}
	else {
		CFRelease(errorString);
	}
	
	CFShow(plistref);
	//add get info
	if (plistref != NULL && CFGetTypeID(plistref) == CFDictionaryGetTypeID())
	{
		CFDictionaryRef dict = (CFDictionaryRef)plistref;

		if (CFDictionaryContainsKey(dict, CFSTR("CarrierId")))
		{
			CFShow((CFStringRef)CFDictionaryGetValue(dict, CFSTR("CarrierId")));
			LPTSTR lpiccid = ConvertData((CFStringRef)CFDictionaryGetValue(dict, CFSTR("CarrierId")));
			OutputDebugString(lpiccid);
			wsprintf(szRet, L"CarrierId=%s\n", lpiccid);
			_tprintf(szRet);
			TCHAR szIni[MAX_PATH] = { 0 };
			GetModuleFileName(NULL, szIni, MAX_PATH);
			CString sIniPath = szIni;
			int i = sIniPath.ReverseFind(L'\\');
			szIni[i] = 0;
			PathCombine(szIni, szIni, _T("CarrierBundle.ini"));
			TCHAR szRetBuf[MAX_PATH] = { 0 };
			GetPrivateProfileString(_T("MobileDeviceCarriersByCarrierID"), lpiccid, _T("Unkown"), szRetBuf, MAX_PATH, szIni);
			TCHAR szKey[MAX_PATH] = { 0 };
			_stprintf_s(szKey, MAX_PATH, _T("com.apple.%s\0"), szRetBuf);
			GetPrivateProfileString(_T("carrierName"), szKey, _T("Unkown"), szRetBuf, MAX_PATH, szIni);
			wsprintf(szRet, L"LastCarrier=%s\n", szRetBuf);
			free(lpiccid);

		}
		else if (CFDictionaryContainsKey(dict, CFSTR("ICCID")))
		{
			CFShow((CFStringRef)CFDictionaryGetValue(dict, CFSTR("ICCID")));
			
			LPTSTR lpiccid = ConvertData((CFStringRef)CFDictionaryGetValue(dict, CFSTR("ICCID")));
			OutputDebugString(lpiccid);
			wsprintf(szRet, L"ICCID=%s\n", lpiccid);
			_tprintf(L"BACK_%s",szRet);
			TCHAR szIni[MAX_PATH] = { 0 };
			GetModuleFileName(NULL, szIni, MAX_PATH);
			CString sIniPath = szIni;
			int i = sIniPath.ReverseFind(L'\\');
			szIni[i] = 0;
			PathCombine(szIni, szIni, _T("CarrierBundle.ini"));
			for (int i = 5; i < 15; i++)
			{
				TCHAR szRetBuf[MAX_PATH] = { 0 };
				CString s = lpiccid;
				
				GetPrivateProfileString(_T("MobileDeviceCarriers"), s.Left(i).GetString(), _T(""), szRetBuf, MAX_PATH, szIni);
				if (_tcslen(szRetBuf)> 0)
				{

					TCHAR szKey[MAX_PATH] = { 0 };
					_stprintf_s(szKey, MAX_PATH, _T("com.apple.%s\0"), szRetBuf);
					GetPrivateProfileString(_T("carrierName"), szKey, _T("Unkown"), szRetBuf, MAX_PATH, szIni);
					wsprintf(szRet, L"LastCarrier=%s\n", szRetBuf);
					break;
				}
				GetPrivateProfileString(_T("MobileDeviceCarriersByMccMnc"), s.Left(i).GetString(), _T(""), szRetBuf, MAX_PATH, szIni);
				if (_tcslen(szRetBuf)> 0)
				{
					TCHAR szKey[MAX_PATH] = { 0 };
					_stprintf_s(szKey, MAX_PATH, _T("com.apple.%s\0"), szRetBuf);
					GetPrivateProfileString(_T("carrierName"), szKey, _T("Unkown"), szRetBuf, MAX_PATH, szIni);
					wsprintf(szRet, L"LastCarrier=%s\n", szRetBuf);
					break;
				}
			}
			free(lpiccid);
		}
		else
		{
			// can not find the carrier info
		
		}
	}

	CFRelease(sFileName);
	CFRelease(plistref);
}